#ifndef __GL_PARSER_H__
#define __GL_PARSER_H__

#include "gl_protocol.h"

void parse_system_frame(PARSE_STRUCT *parser_s);
void parse_flash_frame(PARSE_STRUCT *parser_s);

#endif
