|   HEAD1    |   HEAD2    |   VERSION |   LEN_L   |   LEN_H   |     MasterCmd   |   SlaveCmd    |     DATA    |   CHECKSUM_L    |   CHECKSUM_H    |
|   'G'      |   'L'      |   -       |   LEN_L   |   LEN_H   |     MasterCmd   |   SlaveCmd    |   MAX:4096  |   CHECKSUM_L    |   CHECKSUM_H    |
|    1       |    1       |     1     |    1      |     1     |        1        |       1       |    1-4096   |        1        |       1         |

LEN = DATA LEN
CHECKSUM = VERSION + LEN_L + LEN_H + MasterCmd + SlaveCmd + DATA