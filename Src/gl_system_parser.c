#include "gl_parser.h"
#include "gl_protocol.h"

void parse_system_frame(PARSE_STRUCT *parser_s)
{
    uint8_t version[3] = {VERSION_1,VERSION_2,VERSION_3};
    uint8_t error_code;
    FRAME_STRUCT frame_s;
    frame_s.Version = PROTOCOL_VERSION;
    frame_s.FrameDataLen = 0;
    frame_s.frame_data = NULL;
    frame_s.send_frame_fun = parser_s->frame_s.send_frame_fun;
    frame_s.MasterCmd = MASTER_CMD_SYSTEM;

    if(parser_s->frame_s.MasterCmd != MASTER_CMD_SYSTEM)
    {
        frame_s.SlaveCmd = SLAVE_CMD_SYSTEM_ERROR;
    }else{
        switch(parser_s->frame_s.SlaveCmd)
        {
            case SLAVE_CMD_SYSTEM_REBOOT:

            NVIC_SystemReset(); 

            break;
            case SLAVE_CMD_SYSTEM_GET_VERSION:
            frame_s.SlaveCmd = SLAVE_CMD_SYSTEM_GET_VERSION_SUCCESS;
            frame_s.FrameDataLen = 3;
            frame_s.frame_data = version;
            break;        
            default:
            frame_s.SlaveCmd = SLAVE_CMD_SYSTEM_ERROR;
            break;
        }
    }

    creat_send_cmd(&frame_s);
}