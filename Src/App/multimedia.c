#include "multimedia.h"
#include "audio.h"
#include "video.h"

struct MEDIA_STRUCT
{
	uint32_t audio_offset;
	uint32_t audio_size;
	uint32_t video_offset;
	uint32_t video_size;
};

struct MEDIA_STRUCT media_s;

static void media_read_head(void);

void StartMultimediaTask(void const * argument)
{
    osThreadDef(audioTask, StartAudioTask, osPriorityNormal, 0, 128);
  	osThreadCreate(osThread(audioTask), NULL);

    StartVideoTimer();

    media_read_head();
    audio_play(media_s.audio_offset,media_s.audio_size);
    video_play(media_s.video_offset,media_s.video_size);

    for(;;)
	{
        osDelay(1000);
	}
}

static void media_read_head(void)
{
	uint8_t head[4*4],count = 0;
	SPI_Flash_Read(head,0,16);

	media_s.audio_offset = (uint32_t)(head[count++]<<24);
	media_s.audio_offset |= (uint32_t)(head[count++]<<16);
	media_s.audio_offset |= (uint32_t)(head[count++]<<8);
	media_s.audio_offset |= (uint32_t)(head[count++]<<0);

	media_s.audio_size = (uint32_t)(head[count++]<<24);
	media_s.audio_size |= (uint32_t)(head[count++]<<16);
	media_s.audio_size |= (uint32_t)(head[count++]<<8);
	media_s.audio_size |= (uint32_t)(head[count++]<<0);

	media_s.video_offset = (uint32_t)(head[count++]<<24);
	media_s.video_offset |= (uint32_t)(head[count++]<<16);
	media_s.video_offset |= (uint32_t)(head[count++]<<8);
	media_s.video_offset |= (uint32_t)(head[count++]<<0);

	media_s.video_size = (uint32_t)(head[count++]<<24);
	media_s.video_size |= (uint32_t)(head[count++]<<16);
	media_s.video_size |= (uint32_t)(head[count++]<<8);
	media_s.video_size |= (uint32_t)(head[count++]<<0);

}
