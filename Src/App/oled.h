#ifndef __OLED_H__
#define __OLED_H__

#include "main.h"

//OLED模式设置
//0:4线串行模式
//1:并行8080模式
#define OLED_MODE 0
#define SIZE 16
#define XLevelL		0x00
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF 
#define X_WIDTH 	128
#define Y_WIDTH 	64	  

void oled_init(void);
void oled_show_string(uint8_t x,uint8_t y,uint8_t *chr);

#define OLED_RST_Clr() 	HAL_GPIO_WritePin(GPIOB, OLED_RES_Pin, GPIO_PIN_RESET)//RES
#define OLED_RST_Set() 	HAL_GPIO_WritePin(GPIOB, OLED_RES_Pin, GPIO_PIN_SET)

#define OLED_DC_Clr() 	HAL_GPIO_WritePin(GPIOB, OLED_DC_Pin, GPIO_PIN_RESET)//DC
#define OLED_DC_Set() 	HAL_GPIO_WritePin(GPIOB, OLED_DC_Pin, GPIO_PIN_SET)
 		     
#define OLED_CS_Clr()  	HAL_GPIO_WritePin(GPIOB, OLED_CS_Pin, GPIO_PIN_RESET)//CS
#define OLED_CS_Set()  	HAL_GPIO_WritePin(GPIOB, OLED_CS_Pin, GPIO_PIN_SET)

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLEDDrawAll(unsigned char BMP[],uint16_t len);

#endif

