#ifndef __AUDIO_H__
#define __AUDIO_H__

#include "main.h"

void StartAudioTask(void const * argument);
void audio_play(uint32_t offset,uint32_t size);

#endif

